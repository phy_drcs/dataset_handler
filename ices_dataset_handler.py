import dataset_handler

class IcesDatasetHandler(dataset_handler.DatasetHandler):
    
    def __init__(self):
        dataset_handler.DatasetHandler.__init__(self)
    
    def read_raw_data(self):
        self.raw_data = "raw_ices"
        
    def process_raw_data(self):
        self.processed_data = "processed_"+self.raw_data