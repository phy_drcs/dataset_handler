class DatasetHandler:
    def __init__(self):
        self.raw_data = None
        self.processed_data = None
        pass
    
    def read_raw_data(self):
        pass
    
    def process_raw_data(self):
        pass
    
    def get_processed_data(self):
        return self.processed_data
        
        
class DatasetMerger:
    def __init__(self, dataset_handlers):
        self.dataset_handlers = dataset_handlers
        
        
    def merge_datasets(self):
        merged_ds = ""
        for ds in self.dataset_handlers:
            merged_ds += ds.get_processed_data()+" "
            
        print(merged_ds)